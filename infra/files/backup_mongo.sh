#!/bin/bash
#set -x

export DOCKER="/usr/bin/docker"
DATE=`date +%Y-%m-%d-%H-%M`
BKP_DIR=/var/backups/phoenix-mongodb
RETENTION=7
mkdir -p $BKP_DIR

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Dump a mongo database from a running container"
            echo " "
            echo "$0 [arguments]"
            echo " "
            echo "options:"
            echo "-h, --help  show brief help"
            echo "-u,         username"
            echo "-p,         password"
            echo "-n,         network"
            echo "-d,         database"
            echo "-r,         regexp to filter running container"
            exit 0
            ;;
        -n)
            shift
            if test $# -gt 0; then
                export NETWORK=$1
            else
                echo "no Network specified"
                exit 1
            fi
            shift
            ;;
        -d)
            shift
            if test $# -gt 0; then
                export DATABASE=$1
            else
                echo "no database specified"
                exit 1
            fi
            shift
            ;;
        -p)
            shift
            if test $# -gt 0; then
                export PASSWORD=$1
            else
                echo "no password specified"
                exit 1
            fi
            shift
            ;;
        -u)
            shift
            if test $# -gt 0; then
                export USERNAME=$1
            else
                echo "no username specified"
                exit 1
            fi
            shift
            ;;
        -r)
            shift
            if test $# -gt 0; then
                export REGEXP=$1
            else
                echo "no regexp specified"
                exit 1
            fi
            shift
            ;;
        *)
            break
            ;;
    esac
done

# Run a mongodb container that get linked to the primary of the replicaset and run the mongodump command on it
$DOCKER run --rm --network $NETWORK --link "$($DOCKER ps --format "{{.Names}}" --filter name="${REGEXP}")":mongop1 -v ${BKP_DIR}:/backup mongo bash -c "mongodump --db ${DATABASE} --username ${USERNAME}  --password ${PASSWORD} --host mongop1:27017 --archive=/backup/${DATABASE}-${DATE}.archive"

# Clean up backups older than $RETENTION
cd $BKP_DIR
find $BKP_DIR -mindepth 1 -maxdepth 1 -mtime "+${RETENTION}" -type f -delete

